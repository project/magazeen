<?php

/**
    * @file page.tpl.php
    * Renders the pages for Magazeen.
*/
?>      
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?> 

  <!--[if IE]>
    <?php print phptemplate_get_ie_styles(); ?>
  <![endif]-->
</head>

<body class="<?php print $body_classes ?>">

  <div id="header">
    <div id="header-inner" class="clearfix">

      <div id="headerleft">
        <?php if ($logo): ?>
          <div id="logo">
            <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo-image" /></a>
          </div><!-- /#logo -->
        <?php endif; ?>
        <?php if ($site_name or $site_slogan): ?>
          <div id="site-title">
            <h2><?php print $site_slogan; ?></h2>
            <h1><span></span><a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h1>
          </div><!-- /#site-title -->
        <?php endif; ?>
      </div><!-- /#headerleft -->

      <?php if ($search_box): ?>
        <div id="headerright">
          <div id="search-box">
            <?php print $search_box; ?>
          </div><!-- /#search-box -->
        </div><!-- /#headerright -->
      <?php endif; ?>			

    </div><!-- /#header-inner -->
  </div><!-- /#header -->

  <?php if ($primary_links or $feed_icons): ?>
    <div id="mainnav">
      <div id="mainnav-inner" class="clearfix">

        <?php if ($primary_links): ?>
          <div id="navlist">
            <?php print menu_tree($menu_name = 'primary-links');  ?>
          </div><!-- /#navlist -->
        <?php endif; ?>

        <?php if ($feed_icons): ?>
          <div id="rss">
            <?php print $feed_icons ?>
          </div><!-- /#rss -->
        <?php endif; ?>

      </div><!-- /#mainnav-inner -->
    </div><!-- /#mainnav -->
  <?php endif; ?>

  <?php if ($top): ?>
    <div id="dock">
      <div id="dock-inner">
        <div class="dock-back clearfix">	
          <?php print $top; ?>
        </div><!-- /.dock-back -->
      </div><!-- /#dock-inner -->
    </div><!-- /#dock -->
  <?php endif; ?>

  <div id="page">
    <div id="page-inner" class="clearfix">

      <div class="main <?php print phptemplate_mainwidth_class($right);?> clearfix">
          <?php if ($messages or $help): ?>
            <?php print $messages; ?>
            <?php print $help; ?>
          <?php endif; ?>

        <?php if ($breadcrumb): ?>
          <?php print $breadcrumb; ?>
        <?php endif; ?>

        <?php if ($tabs): ?>
          <div class="tabs">
            <?php print $tabs; ?>
          </div><!-- /.tabs-->
        <?php endif; ?>
 
       <div class="main-inner">
		      
          <?php if ( $template_files[0] !== 'page-node' ): ?>
            <?php if ($title): ?>
              <div class="post-meta clearfix">
                <h3 class="post-title">
                  <?php print $title; ?>
                </h3>							
                <p class="post-info"></p>
              </div><!-- /.post-meta -->
            <?php endif; ?>
          <?php endif; ?>

          <?php print $content; ?>

        </div><!-- /#main-inner -->		
      </div><!-- /#main -->	

      <?php if ($right): ?>
        <div class="sidebar">
          <?php print $right; ?>
        </div><!-- /#sidebar -->
      <?php endif; ?>	

    </div><!-- /#page-inner -->
  </div><!-- /#page -->

  <?php if ($footerleft or $footerright): ?>
    <div id="footer">
      <div class="footer-inner clearfix">	    

        <?php if ($footerleft): ?>
          <div class="footer-left">
            <?php print $footerleft; ?>
          </div><!-- /.footer-left -->
        <?php endif; ?>

        <?php if ($footerright): ?>
          <div class="footer-right">
            <?php print $footerright; ?>
          </div><!-- /.footer-right -->
        <?php endif; ?>

      </div><!-- /.footer-inner -->
    </div><!-- /#footer -->
	<?php endif; ?>

  <?php if ($footer_message): ?>
    <div id="link-back">
      <div class="footer-message clearfix">
        <?php print $footer_message; ?>		
      </div><!-- /#footer-message -->
    </div><!-- /#link-back -->
  <?php endif; ?>

  <?php print $scripts ?>
	<?php print $closure ?>

</body>
</html>
