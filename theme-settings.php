<?php
/**
 * @file theme-settings.php
 * File with theme settings for Magazeen.
 */

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function phptemplate_settings($saved_settings) {

  // Add javascript to show/hide optional settings
  drupal_add_js(drupal_get_path('theme', 'magazeen') . '/script/theme-settings.js', 'theme');

  /**
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the template.php file.
   */
  $defaults = array(
    'breadcrumb_display'                    => 'yes',
    'breadcrumb_separator'                  => ' › ',
    'breadcrumb_home'                       => 1,
    'breadcrumb_trailing'                   => 0,
    'breadcrumb_title'                      => 0,
    'user_notverified_display'              => 1,
    'search_snippet'                        => 1,
    'search_info_type'                      => 1,
    'search_info_user'                      => 1,
    'search_info_date'                      => 1,
    'search_info_comment'                   => 1,
    'search_info_upload'                    => 1,
    'comment_title'                         => 'Leave a Response',
    'display_author'                        => 1,
    'display_teaser_comment'                => 'normal',
    'front_page_title_display'              => 'title_slogan',
    'page_title_display_custom'             => '',
    'other_page_title_display'              => 'ptitle_stitle',
    'other_page_title_display_custom'       => '',
    'configurable_separator'                => ' | ',
    'meta_keywords'                         => '',
    'meta_description'                      => '',
    'rebuild_registry'                      => 0,
    'block_editing'                         => 1,
  );  
    
  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);
  
  
  // Create theme settings form widgets using Forms API
  
  // Breadcrumb
  $form['tnt_container']['general_settings']['breadcrumb'] = array(
    '#type' => 'fieldset',
    '#title' => t('Breadcrumb'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['tnt_container']['general_settings']['breadcrumb']['breadcrumb_display'] = array(
    '#type'          => 'select',
    '#title'         => t('Display breadcrumb'),
    '#default_value' => $settings['breadcrumb_display'],
    '#options'       => array(
                          'yes'   => t('Yes'),
                          'admin' => t('Only in admin section'),
                          'no'    => t('No'),
                        ),
  );
  $form['tnt_container']['general_settings']['breadcrumb']['breadcrumb_separator'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Breadcrumb separator'),
    '#description'   => t('Text only. Don’t forget to include spaces.'),
    '#default_value' => $settings['breadcrumb_separator'],
    '#size'          => 5,
    '#maxlength'     => 10,
    '#prefix'        => '<div id="div-breadcrumb-collapse">', // jquery hook to show/hide optional widgets
  );
  $form['tnt_container']['general_settings']['breadcrumb']['breadcrumb_home'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show home page link in breadcrumb'),
    '#default_value' => $settings['breadcrumb_home'],
  );
  $form['tnt_container']['general_settings']['breadcrumb']['breadcrumb_trailing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append a separator to the end of the breadcrumb'),
    '#default_value' => $settings['breadcrumb_trailing'],
    '#description'   => t('Useful when the breadcrumb is placed just before the title.'),
  );
  $form['tnt_container']['general_settings']['breadcrumb']['breadcrumb_title'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append the content title to the end of the breadcrumb'),
    '#default_value' => $settings['breadcrumb_title'],
    '#description'   => t('Useful when the breadcrumb is not placed just before the title.'),
    '#suffix'        => '</div>', // #div-breadcrumb
  );

  // Username
  $form['tnt_container']['general_settings']['username'] = array(
    '#type' => 'fieldset',
    '#title' => t('Username'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['tnt_container']['general_settings']['username']['user_notverified_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display "not verified" for unregistered usernames'),
    '#default_value' => $settings['user_notverified_display'],
  );
  // Search Settings
  $form['tnt_container']['general_settings']['search_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search results'),
    '#description' => t('What additional information should be displayed on your search results page?'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['tnt_container']['general_settings']['search_container']['search_results']['search_snippet'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display text snippet'),
    '#default_value' => $settings['search_snippet'],
  );
  $form['tnt_container']['general_settings']['search_container']['search_results']['search_info_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display content type'),
    '#default_value' => $settings['search_info_type'],
  );
  $form['tnt_container']['general_settings']['search_container']['search_results']['search_info_user'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display author name'),
    '#default_value' => $settings['search_info_user'],
  );
  $form['tnt_container']['general_settings']['search_container']['search_results']['search_info_date'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display posted date'),
    '#default_value' => $settings['search_info_date'],
  );
  $form['tnt_container']['general_settings']['search_container']['search_results']['search_info_comment'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display comment count'),
    '#default_value' => $settings['search_info_comment'],
  );
  $form['tnt_container']['general_settings']['search_container']['search_results']['search_info_upload'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display attachment count'),
    '#default_value' => $settings['search_info_upload'],
  );

  // Page setting
  $form['tnt_container']['general_settings']['page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page setting'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['tnt_container']['general_settings']['page']['display_author'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display author name'),
    '#default_value' => $settings['display_author'],
  );

  // Comment
  $form['tnt_container']['general_settings']['comment'] = array(
    '#type' => 'fieldset',
    '#title' => t('Comment setting'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Comment Title
  $form['tnt_container']['general_settings']['comment']['comment_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Comment Title'),
    '#description' => t('Customize the title for the comments box'),
    '#size' => 60,
    '#default_value' => $settings['comment_title'],
  );

  // Display Teaser Comment
  $form['tnt_container']['general_settings']['comment']['display_teaser_comment'] = array(
    '#type'          => 'select',
    '#title'         => t('Option to display comment count in teaser if less than 1'),
    '#default_value' => $settings["display_teaser_comment"],
    '#options'       => array(
                          'normal' => t('Display comment count'),
                          'hide' => t('Don\'t display comment count'),
                          'addnew' => t('Display "Add new comment"'),
  ),
  );

  // SEO settings
  $form['tnt_container']['seo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search engine optimization (SEO) settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Page titles
  $form['tnt_container']['seo']['page_format_titles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page titles'),
    '#description'   => t('This is the title that displays in the title bar of your web browser. Your site title, slogan, and mission can all be set on your Site Information page'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // front page title
  $form['tnt_container']['seo']['page_format_titles']['front_page_format_titles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Front page title'),
    '#description'   => t('Your front page in particular should have important keywords for your site in the page title'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['tnt_container']['seo']['page_format_titles']['front_page_format_titles']['front_page_title_display'] = array(
    '#type' => 'select',
    '#title' => t('Set text of front page title'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#default_value' => $settings['front_page_title_display'],
    '#options' => array(
                    'title_slogan' => t('Site title | Site slogan'),
                    'slogan_title' => t('Site slogan | Site title'),
                    'title_mission' => t('Site title | Site mission'),
                    'custom' => t('Custom (below)'),
                  ),
  );
  $form['tnt_container']['seo']['page_format_titles']['front_page_format_titles']['page_title_display_custom'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom'),
    '#size' => 60,
    '#default_value' => $settings['page_title_display_custom'],
    '#description'   => t('Enter a custom page title for your front page'),
  );
  // other pages title
  $form['tnt_container']['seo']['page_format_titles']['other_page_format_titles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other page titles'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['tnt_container']['seo']['page_format_titles']['other_page_format_titles']['other_page_title_display'] = array(
    '#type' => 'select',
    '#title' => t('Set text of other page titles'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#default_value' => $settings['other_page_title_display'],
    '#options' => array(
                    'ptitle_slogan' => t('Page title | Site slogan'),
                    'ptitle_stitle' => t('Page title | Site title'),
                    'ptitle_smission' => t('Page title | Site mission'),
                    'ptitle_custom' => t('Page title | Custom (below)'),
                    'custom' => t('Custom (below)'),
                  ),
  );
  $form['tnt_container']['seo']['page_format_titles']['other_page_format_titles']['other_page_title_display_custom'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom'),
    '#size' => 60,
    '#default_value' => $settings['other_page_title_display_custom'],
    '#description'   => t('Enter a custom page title for all other pages'),
  );

  // SEO configurable separator
  $form['tnt_container']['seo']['page_format_titles']['configurable_separator'] = array(
    '#type' => 'textfield',
    '#title' => t('Title separator'),
    '#description' => t('Customize the separator character used in the page title'),
    '#size' => 60,
    '#default_value' => $settings['configurable_separator'],
  );

  // Metadata
  $form['tnt_container']['seo']['meta'] = array(
    '#type' => 'fieldset',
    '#title' => t('Meta tags'),
    '#description' => t('Meta tags aren\'t used much by search engines anymore, but the meta description is important -- this is what will be shown as the description of your link in search engine results.  NOTE: For more advanced meta tag functionality, check out the Meta Tags (aka. Node Words) module.  These theme settings do not work in conjunction with this module and will not appear if you have it enabled.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  if (module_exists('nodewords') == FALSE) {
    $form['tnt_container']['seo']['meta']['meta_keywords'] = array(
      '#type' => 'textfield',
      '#title' => t('Meta keywords'),
      '#description' => t('Enter a comma-separated list of keywords'),
      '#size' => 60,
      '#default_value' => $settings['meta_keywords'],
    );
    $form['tnt_container']['seo']['meta']['meta_description'] = array(
      '#type' => 'textarea',
      '#title' => t('Meta description'),
      '#cols' => 60,
      '#rows' => 6,
      '#default_value' => $settings['meta_description'],
    );
   } 
   else {
      $form['tnt_container']['seo']['meta']['#description'] = 'NOTICE: You currently have the "nodewords" module installed and enabled, so the meta tag theme settings have been disabled to prevent conflicts.  If you later wish to re-enable the meta tag theme settings, you must first disable the "nodewords" module.';
      $form['tnt_container']['seo']['meta']['meta_keywords']['#disabled'] = 'disabled';
      $form['tnt_container']['seo']['meta']['meta_description']['#disabled'] = 'disabled';
  }

  // Theme Dev Setting
  $form['tnt_container']['theme_dev'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme development settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Rebuild Registry
  $form['tnt_container']['theme_dev']['rebuild_registry'] = array(
    '#type' => 'checkbox',
    '#title'         => t('Rebuild theme registry on every page.'),
    '#description'   => t('During theme development, it can be very useful to continuously <a href="!link">rebuild the theme registry</a>. WARNING: this is a huge performance penalty and must be turned off on production websites.', array('!link' => 'http://drupal.org/node/173880#theme-registry')),
    '#default_value' => $settings['rebuild_registry'],
  );

  // Block Editing
  $form['tnt_container']['theme_dev']['block_editing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show block editing on hover'),
    '#description'   => t('When hovering over a block, privileged users will see block editing links.'),
    '#default_value' => $settings['block_editing'],
  );

  // Return theme settings form
  return $form;
}  

