<?php
/**
 * @file node.tpl.php
 * Renders node for Magazeen.
*/
?>

<div id="post-<?php print $node->nid; ?>" class="post clearfix 
  <?php if ($sticky) { 
    print " sticky"; 
  } ?>
  <?php if (!$status) {
    print " node-unpublished"; } 
  ?>
">

  <div class="post-meta clearfix">
    <h3 class="post-title">
      <a href="<?php print $node_url?>"><?php print $title?></a>
    </h3>
  <p class="post-info">
    <?php 
      if ($type == page) {
        print phptemplate_display_author($name) ; 
      } 
      else {
        print '<span>'. t('By') .' '. $name .'</span>';
      } 
    ?>
    <?php 
      if ($type == page) { 
        print ''; 
      }
      else {
        print '<span class="date">'. $date .'</span>';
      } 
    ?>
		</p>
	</div><!-- /post-meta -->

  <div class="post-box">
	  <div class="clearfix post-content 
      <?php 
        if ($teaser == 1) { 
          print t('teaser'); 
        } 
        else {
          print t('full'); 
        }
      ?>">

		  <?php if ($teaser == 1): ?>
				<?php if ($comment_count > 1): ?>				
				  <div class="comment-count">
					  <a title="
					  <?php 
					    print $comment_count; 
					    if ($comment_count > 1) { 
					      print t('Comments'); 
					    } 
					    else { 
					      print t('Comment'); 
					    } 
					  ?> 					 " href="<?php print $node_url .'#comments' ?>">
					  <?php 
					    print $comment_count; 
					    if ($comment_count > 1) { 
					      print ' ' . t('Comments'); 
					    } 
					    else { 
					      print ' ' . t('Comment'); 
					    } 
					  ?> 
					  </a>
          </div>				
        <?php else : ?>
          <?php print phptemplate_comment_count($comment_count, $node_url); ?>
        <?php endif; ?>	
					  	
        <?php if ($field_image_rendered): ?>
          <div class="post-image">
            <?php print $field_image_rendered; ?>
          </div><!-- /comment-count -->
        <?php endif; ?>
		 <?php endif; ?>

	  	<div class="post-intro clearfix">
		  	<?php if ($teaser == 0): ?>
		  		<?php if ($field_image): ?>
						<div class="post-image-inner">
							<?php print $field_image_rendered; ?>
						</div>
					<?php endif; ?>
				<?php endif; ?>
					<?php print $content?>
			</div><!-- /post-intro -->
		</div><!-- /post-content -->
		
		<?php if ($teaser == 1): ?>
			<div class="post-footer clearfix">
				<div class="continue-reading">
					<a title="<?php print t('Continue reading'); ?> &#8220;<?php print $title?>&#8220;" rel="bookmark" href="<?php print $node_url?>"><?php print t('Continue reading'); ?></a>
				</div>
	      <?php if ($terms): ?>
	        <div class="category-menu"><span class="indicator"></span><?php print $terms; ?></div>
	      <?php endif; ?>
			</div><!-- /post-footer -->
		<?php endif; ?>
  </div><!-- /post-box -->

</div>

