<?php

/**
 * @file comment.tpl.php
 * Default theme implementation for comments.
 *
 * Available variables:
 * - $author: Comment author. Can be link or plain text.
 * - $content: Body of the post.
 * - $date: Date and time of posting.
 * - $links: Various operational links.
 * - $new: New comment marker.
 * - $picture: Authors picture.
 * - $signature: Authors signature.
 * - $status: Comment status. Possible values are:
 *   comment-unpublished, comment-published or comment-preview.
 * - $submitted: By line with date and time.
 * - $title: Linked title.
 *
 * These two variables are provided for context.
 * - $comment: Full comment object.
 * - $node: Node object the comments are attached to.
 *
 * @see template_preprocess_comment()
 * @see theme_comment()
 */
?>
<div class="comment<?php print ' '. $zebra; print($comment->new) ? ' comment-new' : ''; print ' '. $status ?> clear-block">

  <div class="comment-author clearfix">
    <?php print $picture ?>
      <div class="author">
        <strong class="name">
          <?php print $author; ?>
        </strong> 
        <?php if ($comment->new): ?>
          <span class="new"><?php print $new; ?></span>
        <?php endif; ?>	
          <span class="date"> <?php print t(on). ' ' .$date; ?></span>
	  	</div>
    <?php if ($links): ?>
      <div class="comment-links">
        <?php print $links; ?>
      </div>
    <?php endif; ?>
  </div>

  <div class="content">
    <?php print $content ?>
    <?php if ($signature): ?>
      <div class="user-signature clear-block">
        <?php print $signature ?>
      </div>
    <?php endif; ?>
  </div>

</div>
